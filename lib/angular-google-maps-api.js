'use strict';

var $maps_api = require("angular").module('ngGmapsAPI', []);
        
$maps_api.factory('GmapMapScriptLoader', [
    '$q', 'uiGmapuuid', function($q, uuid) {
        var getScriptUrl, includeScript, isGoogleMapsLoaded, scriptId;
        scriptId = void 0;
        getScriptUrl = function(options) {
            if (options.china) {
                return 'http://maps.google.cn/maps/api/js?';
            } else {
                if (options.transport === 'auto') {
                    return '//maps.googleapis.com/maps/api/js?';
                } else {
                    return options.transport + '://maps.googleapis.com/maps/api/js?';
                }
            }
        };
        includeScript = function(options) {
            var omitOptions, query, script;
            omitOptions = ['transport', 'isGoogleMapsForWork', 'china'];
            if (options.isGoogleMapsForWork) {
                omitOptions.push('key');
            }
            query = _.map(_.omit(options, omitOptions), function(v, k) {
                return k + '=' + v;
            });
            if (scriptId) {
                document.getElementById(scriptId).remove();
            }
            query = query.join('&');
            script = document.createElement('script');
            script.id = scriptId = "ui_gmap_map_load_" + (uuid.generate());
            script.type = 'text/javascript';
            script.src = getScriptUrl(options) + query;
            return document.body.appendChild(script);
        };
        isGoogleMapsLoaded = function() {
            return angular.isDefined(window.google) && angular.isDefined(window.google.maps);
        };
        return {
            load: function(options) {
                var deferred, randomizedFunctionName;
                deferred = $q.defer();
                if (isGoogleMapsLoaded()) {
                    deferred.resolve(window.google.maps);
                    return deferred.promise;
                }
                randomizedFunctionName = options.callback = 'onGoogleMapsReady' + Math.round(Math.random() * 1000);
                window[randomizedFunctionName] = function() {
                    window[randomizedFunctionName] = null;
                    deferred.resolve(window.google.maps);
                };
                if (window.navigator.connection && window.Connection && window.navigator.connection.type === window.Connection.NONE) {
                    document.addEventListener('online', function() {
                        if (!isGoogleMapsLoaded()) {
                            return includeScript(options);
                        }
                    });
                } else {
                    includeScript(options);
                }
                return deferred.promise;
            }
        };
    }
]);

$maps_api.provider('GmapGoogleMapApi', function() {
    this.options = {
        transport: 'https',
        isGoogleMapsForWork: false,
        china: false,
        v: '3.17',
        libraries: '',
        language: 'en',
        sensor: 'false'
    };
    this.configure = function(options) {
        angular.extend(this.options, options);
    };
    this.$get = [
        'GmapMapScriptLoader', '$q', (function(_this) {
            return function(loader, $q) {
                var config_deferred = $q.defer();
                var loadAPI_deferred;
                
                return {
                    configure: function(options){
                        _this.configure(options);
                        config_deferred.resolve();
                    },
                    
                    loadAPI: function(){
                        
                        if(loadAPI_deferred){
                            return loadAPI_deferred.promise;
                        }
                        
                        loadAPI_deferred = $q.defer();
    
                        config_deferred.promise.whenResolved(function(){
                            loader.load(_this.options).then(function(maps){
                                loadAPI_deferred.resolve(maps);
                            });
                        });
                        
                        return loadAPI_deferred.promise;
                    }
                };
            };
        })(this)
    ];
    
    return this;
});    