'use strict';

require("./angular-google-autocomplete");
require("./angular-google-maps-api");
require("./angular-google-maps");

//Package everything into one angular module.
require("angular").module("riAngularGmaps", [
    'ngGmapsAPI', 
    'uiGmapgoogle-maps',
    'ngGoogleAutocomplete'
]);