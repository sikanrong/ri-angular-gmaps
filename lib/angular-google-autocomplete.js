'use strict';

/**
 * A directive for adding google places autocomplete to a text box
 * google places autocomplete info: https://developers.google.com/maps/documentation/javascript/places
 *
 * Usage:
 *
 * <input type="text"  ng-autocomplete ng-model="autocomplete" options="options" details="details/>
 *
 * + ng-model - autocomplete textbox value
 *
 * + details - more detailed autocomplete result, includes address parts, latlng, etc. (Optional)
 *
 * + options - configuration for the autocomplete (Optional)
 *
 *       + types: type,        String, values can be 'geocode', 'establishment', '(regions)', or '(cities)'
 *       + bounds: bounds,     Google maps LatLngBounds Object, biases results to bounds, but may return results outside these bounds
 *       + country: country    String, ISO 3166-1 Alpha-2 compatible country code. examples; 'ca', 'us', 'gb'
 *       + watchEnter:         Boolean, true; on Enter select top autocomplete result. false(default); enter ends autocomplete
 *
 * example:
 *
 *    options = {
 *        types: '(cities)',
 *        country: 'ca'
 *    }
**/

require("angular").module( "ngGoogleAutocomplete", ["ngGmapsAPI"])

.factory("ngGooglePlaces", ["GmapGoogleMapApi", "$q", function(places_api, $q){
    return function(places_opts){
        var service_deferred = $q.defer();
    
        places_api.loadAPI().then(function(maps){
            service_deferred.resolve({

                getPlacesResults: function(query){
                    var deferred = $q.defer();    
                    var autocompleteService = new maps.places.AutocompleteService();
                    if (query.length > 0){
                      autocompleteService.getPlacePredictions(
                        angular.extend({
                          input: query,
                          offset: query.length
                        }, places_opts),
                        function listentoresult(list, status) {
                            if(list == null || list.length == 0) {
                                deferred.reject("no_results");
                            } else {
                                deferred.resolve(list);
                            }
                        });
                    }else{
                        deferred.reject("query_empty");
                    }

                    return deferred.promise;
                },

                //function to get retrieve the autocompletes first result using the AutocompleteService 
                getFirstPlacesResult: function(query){
                    var deferred = $q.defer();    

                    this.getPlacesResults(query).then(function(list){

                        var placesService = new maps.places.PlacesService($("<div></div>")[0]);
                        placesService.getDetails(
                          {'reference': list[0].reference},
                          function detailsresult(detailsResult, placesServiceStatus){
                            if (placesServiceStatus == maps.GeocoderStatus.OK){
                                deferred.resolve(detailsResult);
                            }else{
                                deferred.reject("status_not_ok");
                            }
                        });
                    }, function(reason){
                        deferred.reject(reason);
                    });

                    return deferred.promise;
                }

            });
        });

        return service_deferred.promise;
    }
}])

.directive('ngGoogleAutocomplete', [
    "GmapGoogleMapApi", 
    "ngGooglePlaces", 
    "$q", 
    function(
        places_api, 
        places_service, 
        $q) {
    return {
      require: 'ngModel',
      scope: {
        ngModel: '=',
        options: '=?',
        details: '=?',
        control: '=?'
      },

      link: function(scope, element, attrs, controller) {
          
        scope.autocomConfig = {};
          
        places_api.loadAPI().then(function(maps){
            
            var autocompleteListener;
            
            function disableGoogleAutocomplete() {
                google.maps.event.removeListener(autocompleteListener);
                google.maps.event.clearInstanceListeners(scope.gPlace);
                $(document).livequery(".pac-container", function(el){el.remove();});
            }
            
            function enableGoogleAutocomplete() {
                
                scope.gPlace = new google.maps.places.Autocomplete(element[0], scope.autocomConfig);
                
                autocompleteListener = google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                    var result = scope.gPlace.getPlace();
                    if (result !== undefined) {
                        if (result.address_components !== undefined) {
                            scope.$apply(function() {
                                scope.details = result;
                                controller.$setViewValue(element.val());
                            });
                        } else {
                            if (watchEnter) {
                              scope.selectFirstResult(result.name);
                            }
                        }
                    }
                });
            }
            
            //options for autocomplete
            var opts
            var watchEnter = false
            //convert options provided to opts
            var initOpts = function() {

              opts = {}
              if (scope.options) {

                if (scope.options.watchEnter !== true) {
                  watchEnter = false
                } else {
                  watchEnter = true
                }

                if (scope.options.types) {
                  opts.types = []
                  opts.types.push(scope.options.types)
                  scope.gPlace.setTypes(opts.types)
                } else {
                  scope.gPlace.setTypes([])
                }

                if (scope.options.bounds) {
                  opts.bounds = scope.options.bounds
                  scope.gPlace.setBounds(opts.bounds)
                } else {
                  scope.gPlace.setBounds(null)
                }

                if (scope.options.country) {
                  opts.componentRestrictions = {
                    country: scope.options.country
                  }
                  scope.gPlace.setComponentRestrictions(opts.componentRestrictions)
                } else {
                  scope.gPlace.setComponentRestrictions(null)
                }
                
                if(scope.options.disabled){
                    disableGoogleAutocomplete();
                }else{
                    enableGoogleAutocomplete();
                }
                
              }
            }

            controller.$render = function () {
              var location = controller.$viewValue;
              element.val(location);
            };
            
            scope.selectFirstResult = function(query){
                var deferred = $q.defer();
                
                places_service(scope.autocomConfig).then(function(service){
                    service.getFirstPlacesResult(query).then(function(detailsResult){
                        scope.details = detailsResult;
                        controller.$setViewValue(detailsResult.formatted_address);
                        controller.$render();
                        deferred.resolve(detailsResult);
                        
                    }, function(reason){
                        scope.details = null;
                        deferred.reject(reason);
                    });
                });
                
                return deferred.promise;
            };

            //watch options provided to directive
            scope.watchOptions = function () {
              return scope.options
            };
            
            scope.init = function(){
                if(scope.control){
                    angular.extend(scope.control, {
                        selectFirstResult: scope.selectFirstResult
                    });
                }


                var init_opts = {};
                if(scope.options && 
                   scope.options.autocompleteInitOpts){
                   init_opts = scope.options.autocompleteInitOpts;

                   if(init_opts.bounds){
                        var cityCenter = new maps.LatLng(init_opts.bounds.coordinate.lat, init_opts.bounds.coordinate.lng);
                        var circle = new maps.Circle({radius: init_opts.bounds.radius, center: cityCenter});
                        scope.autocomConfig = angular.extend(scope.autocomConfig, {bounds: circle.getBounds()});
                   }
                }

                enableGoogleAutocomplete();
                
                scope.$watch(scope.watchOptions, function () {
                    initOpts()
                }, true);
            };
            
            scope.init();
            
        });
      }
    };
  }]);